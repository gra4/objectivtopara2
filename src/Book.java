public class Book {
    private int Year;
    private String Name, Author;
    public Book (String Name, String Author, int Year){
        this.Author = Author;
        this.Name = Name;
        this.Year = Year;
    }

    public String getName() {
        return Name;
    }

    public int getYear() {
        return Year;
    }

    public String getAuthor() {
        return Author;
    }

    public String toString(){
        return String.format("Название: %s\nАвтор: %s\nГод написания: %d\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n", Name, Author, Year);
    }

}

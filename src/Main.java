import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int g = 1, d = 0;
        int a;
        int name;
        List<Book> Liabrary = new ArrayList<>();
        Liabrary.add(new Book("Три товарища", "Э.М. Ремарк", 1954 ));
        Liabrary.add(new Book("Кольцо силы", "Карлос Кастанеда", 1983));
        Liabrary.add(new Book("МиФы и легенды древней Греции", "Н.А. Кун", 1977));
        Liabrary.add(new Book("Преступление и наказание", "Ф.М. Достоевский", 1855));
        Liabrary.add(new Book("1984", "Джордж Оруэлл", 1933));
        Liabrary.add(new Book("О дивный новый мир", "Олдос Хаксли", 1935));
        Liabrary.add(new Book("451 градус по фаренгейту", "Рэй Бредберри", 1953));

        while (true){
            System.out.println("1 - Поиск книги по названию\n2 - Добавить книгу\n3 - Удалить книгу\n4 - Вывод всех книг");
            a = scanner.nextInt();
            switch (a) {
                case 1: {
                    while(g != 0) {
                        System.out.println("Год написания книги: ");
                        name = scanner.nextInt();
                        for (int i = 0; i < Liabrary.size(); i++){
                            if (Liabrary.get(i).getYear() == name) {
                                System.out.printf(Liabrary.get(i).toString());
                                d++;
                            }
                        }
                        if (d == 0) {
                            System.out.printf("В базе нет книги с таким годом написания\n");
                        }
                        System.out.println("Продолжить? (1 - да/2 - нет)");
                        g = scanner.nextInt();
                        switch (g) {
                            case 1:
                                g = 0;
                                break;
                            case 2:
                                g = 1;
                                break;
                        }
                    }
                    break;
                }
                case 2:{
                    String bookname, author;
                    int year;
                    System.out.printf("Автор: ");
                    author = scanner.next();
                    if (author.contains(" "))
                        continue;
                    System.out.printf("Название книги: ");
                    bookname = scanner.next();
                    if (bookname.contains(" "))
                        continue;
                    System.out.printf("Год издания: ");
                    year = scanner.nextInt();
                    Liabrary.add(new Book(author, bookname, year));
                    break;
                }
                case 3:{
                    for (int i = 0; i < Liabrary.size(); i++) {
                        System.out.printf((i + 1) + ". " + Liabrary.get(i));
                    }
                    System.out.printf("ID книги, которую желаете удалить\n");
                    int j = scanner.nextInt();
                    Liabrary.remove(j - 1);
                    break;
                }
                case 4:{
                    for (int i = 0; i < Liabrary.size(); i++) {
                        System.out.printf((i + 1) + ". " + Liabrary.get(i));
                    }
                    break;
                }
            }
        }
    }
}
